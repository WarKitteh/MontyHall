#include <iostream>

void clearScreen() {
	std::cout << "\033[2J\033[1;1H";
}

int randomdoor() {
	int door = rand() % 3 + 1;
	return(door);
}

int main() {
	srand (time(NULL)); // Initialize random seed

	int max_simulations = 1000000;
	int progress;

	int correct_switch = 0;
	int correct_stay = 0;
	int percentage_switch;
	int percentage_stay;
	int door_correct;
	int door_guess;
	int door_remove;

	for (int i = 0; i < max_simulations; i++) {
		door_correct = randomdoor(); // Random number from 1 to 3
		door_guess = randomdoor();

		door_remove = randomdoor();
		while (door_remove == door_guess || door_remove == door_guess) {
			door_remove = randomdoor();
		}
		
		if (door_guess == door_correct) {
			correct_stay++;
		}
		else {
			correct_switch++;
		}

		progress = i * 100 / max_simulations;
		clearScreen();
		std::cout << "Tests done: " << i + 1 << "/" << max_simulations << " (" << progress << "%)\n";
	}
	clearScreen();

	percentage_stay = correct_stay * 100 / max_simulations;
	percentage_switch = correct_switch * 100 / max_simulations;

	std::cout << "Stay: " << correct_stay << " (" << percentage_stay << "%)\n";
	std::cout << "Switch: " << correct_switch << " (" << percentage_switch << "%)";
}
